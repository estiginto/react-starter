/* eslint-disable @next/next/no-img-element */
import React, { useState } from 'react'

import Link from 'next/link';

function magiceImg(width: number, height: number): string {
  return `https://picsum.photos/${width}/${height}`
}

function MyHeader() {
  // 元件的狀態
  const [menuOpen, setMenuOpen] = useState(false);
  return (
    <header className="flex items-center w-full h-16 shadow">
      <img src={magiceImg(50, 50)} className="px-3 ml-3" alt="logo" />
      <h1 className="px-3">Tailwind Example</h1>

      {/* 右邊標題列, 因為 ml-auto 讓左邊無限延長後壓到右手邊了 */}
      <div className="relative mr-3 ml-auto h-full">

        {/* 注意 onClick 執行的內容會把 menuOpen 反向 */}
        <button className="px-3 h-full hover:bg-gray-100" onClick={() => { setMenuOpen(!menuOpen) }}>
          選單
        </button>

        {/* 變數判斷決定是否顯示下拉選單 */}
        {menuOpen && <div className="flex absolute right-0 z-10 flex-col mt-1 w-40 bg-white rounded border shadow-lg">
          <Link href="/examples">
            <a className="py-3 px-7 hover:bg-gray-100">Example 1</a>
          </Link>
          <Link href="/another_example">
            <a className="py-3 px-7 hover:bg-gray-100">Example 2</a>
          </Link>
          <Link href="/">
            <a className="py-3 px-7 hover:bg-gray-100">回首頁</a>
          </Link>
        </div>}
      </div>
    </header>
  )
}

export default MyHeader
