import React, { Children } from 'react'

interface Props {
  children: React.ReactNode;
}

const TitleWithCss = (props: Props) => {
  return (
    <>
      {/* 加上 jsx 之後這個只會應用這在個 Component 裡面 不會傳染到外面 非常好用 */}
      <style jsx>{`
        h1 {
          color: red;
        }
      `}</style>
      <h1>{props.children}</h1>
    </>
  )
}

export default TitleWithCss
