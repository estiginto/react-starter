import React from 'react'
import Head from 'next/head'
import MyHeader from '../components/MyHeader'

function Examples() {
  return (
    <div className="min-h-screen bg-gray-200">
      {/* 這個是 JSX 註解 */}
      <Head>
        {/* Head 代表的是原本 HTML 裡面的 head 要放的東西 */}
        <title>Examples</title>
        <meta name="description" content="Examples" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      {/* 外框的部分 */}
      <div className="container mx-auto min-h-screen bg-white shadow-2xl">
        {/* 標題列 (共用的部分拉出去了) */}
        <MyHeader />
        {/* 主內容 */}
        <main>
          <h1 className='mx-auto mt-16 text-5xl text-center text-blue-700'>我是主要內容1</h1>
        </main>
        {/* 頁尾 */}
        <footer></footer>
      </div>
    </div>
  )
}

export default Examples
