
請 Fork 此專案後開始進行版型開發

[[_TOC_]]


## 根目錄下檔案及資料夾說明

請參考以下說明，沒事不用動的可以先不用理解

資料夾名稱           | 說明
--------------------|:-----
.vscode             | 文字編輯器專案設定檔
pages               | 版型核心 請將所有版型檔案放這
public              | 部分靜態檔案放置區 (請替換 favicon.ico 就好)
styles              | 放 CSS 的地方
.eslintrc           | 語法檢查設定檔 (沒事不用動)
.gitignore          | 版本管理設定檔 (沒事不用動)
.gitpod.yml         | 雲端環境設定檔 (沒事不用動)
next-env.d.ts       | Next.js 設定檔案 (沒事不用動)
next.config.js      | Next.js 設定檔案 (沒事不用動)
package.json        | 套件管理設定檔案 (使用指令管理)
tsconfig.json       | Type Script 設定檔案  (沒事不用動)
yarn.lock           | 套件管理設定檔案 (使用指令管理)


## Page 底下檔案及資料夾說明

```
pages/index.tsx → /
pages/blog/index.tsx → /blog
pages/blog/first-post.tsx → /blog/first-post
pages/dashboard/settings/username.tsx → /dashboard/settings/username
```

檔名 = 網址，詳細可以參考[這個頁面](https://nextjs.org/docs/routing/introduction#index-routes)

## 已預先安裝的插件們

* [tailwindcss](https://tailwindcss.com/)
* [headlessUI](https://headlessui.dev/)
* [heroicons](https://heroicons.com/)

## 考慮預先安裝的插件們

* [redix-ui](https://www.radix-ui.com/) 補齊一些常用動作不含風格
* [Stitches](https://stitches.dev/) 好用的 CSS-in-JS
* ~~[VechaiUI](https://www.vechaiui.com/)~~ (我個人覺得難用)

## 手動建立專案的方式 (非 Fork)

此建立方式可以避免預先安裝的插件們  
執行以下指令並根據提示輸入專案名稱
```
yarn create next-app --typescript
```

從此專案複製檔案 `.gitpod.yml` 及資料夾 `.vscode` 至新專案根目錄下